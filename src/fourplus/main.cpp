/*
	fourplus -- puzzle game about matchin pairs of four
	kellermann@protonmail.com
 */

#include <iomanip>
#include <iostream>

#include <array>
#include <vector>


namespace fourplus {

	using field = std::vector;

	constexpr std::array<char, 3u> const g_symbols {
		'o', 'x', '-'
	};

} // namespace fourplus


int main(int argc, char const** const argv) try {
	std::cout << "hello world\n";
} catch (std::exception const& error) {
	std::cerr << "error: " << error.what() << '\n';
	return EXIT_FAILURE;
} catch (...) {
	std::cerr << "error: unknown\n";
	return EXIT_FAILURE;
}

// ex:ts=4:
